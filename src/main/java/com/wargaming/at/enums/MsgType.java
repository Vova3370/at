package com.wargaming.at.enums;

import java.util.Arrays;

/**
 * Данный enam хранит тип сообщений
 */

public enum MsgType {

    INFO("info"),
    ERROR("error");

    private String strType;

    MsgType(String strType) {
        this.strType = strType;
    }

    public String getStrType() {
        return strType;
    }

    public static MsgType getTypeContainsOf(String strType) {
        return Arrays.asList(MsgType.values()).stream()
                .filter(msgType -> msgType.getStrType().contains(strType))
                .findFirst().get();
    }
}
