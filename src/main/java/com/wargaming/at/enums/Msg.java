package com.wargaming.at.enums;

/**
 * Данный енам хранит сообщения в БП
 */

public enum Msg {

    EMAIL_REQUIRED(MsgType.ERROR, "An email address required."),
    INVALID_EMAIL_ADDRESS(MsgType.ERROR, "Invalid email address."),
    PASSWORD_REQUIRED(MsgType.ERROR, "Password is required."),
    AUTHENTICATION_FAILD(MsgType.ERROR, "Authentication failed."),
    MESSAGE_NO_BLANK(MsgType.ERROR, "The message cannot be blank."),
    MESSAGE_SUCCESSFULLY(MsgType.INFO, "Your message has been successfully sent to our team.");


    private MsgType msgType;
    private String msgText;

    Msg(MsgType msgType, String msgText) {
        this.msgType = msgType;
        this.msgText = msgText;
    }

    public String getMsgText() {
        return msgText;
    }

    public MsgType getMsgType() {
        return msgType;
    }

}

