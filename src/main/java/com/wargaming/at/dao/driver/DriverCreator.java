package com.wargaming.at.dao.driver;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;


public class DriverCreator {

    private final static Logger LOG = Logger.getLogger(DriverCreator.class);

    private static String DRIVER_NAME = "webdriver.chrome.driver";
    private static String DRIVER_PATH = "src/main/recources/driver/chromedriver.exe";
    private final static int IMPLICITY_WAIT = 20;

    private static WebDriver driver;

    public DriverCreator(){

    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void init() {
        System.setProperty(DRIVER_NAME, DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICITY_WAIT, TimeUnit.SECONDS);
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().maximize();
        LOG.info("Переход по url" + driver.toString());

    }

    public static void close() {
        driver.close();
        LOG.info("Завершение.Выход");
    }

}
