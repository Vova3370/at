package com.wargaming.at.dao.util;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;

/**
 * Утилитные методы
 */

public class ExpectedCondition {

    private static int expectation = 10;

    private ExpectedCondition(WebDriver driver) {
    }

    public static WebElement expectedConditionVisibilityOf(int time, WebElement element) {
        new WebDriverWait(getDriver(), time).until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public static WebElement expectedConditionVisibilityOf(WebElement element) {
        new WebDriverWait(getDriver(), expectation).until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public static WebElement expectedConditionVisibilityOf(String s) {
        return new WebDriverWait(getDriver(), expectation).until(ExpectedConditions.visibilityOf(getDriver().findElement(By.id(s))));
    }


    public static void clickWhenReady(WebElement element, int timeout) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeout);
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
        el.click();
    }

    public static void clickWhenReady(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), expectation);
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
        el.click();
    }

    public static void scrollToViewElement(WebElement element) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
