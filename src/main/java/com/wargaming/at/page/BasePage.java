package com.wargaming.at.page;


import com.wargaming.at.model.User;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.Logger;
import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;


public class BasePage {

    private static final Logger logger = Logger.getLogger(BasePage.class);

    @FindBy(xpath = "//*[contains(text(), 'Sign in')]")
    public WebElement login;

    @FindBy(id = "search_query_top")
    public WebElement searhQueryTop;

    @FindBy(name="submit_search")
    public WebElement searhButton;

    @FindBy(className= "logout")
    public WebElement logout;

    public BasePage(){
        PageFactory.initElements(getDriver(), this);
    }


    public BasePage open(){
        expectedConditionVisibilityOf("header_logo");
        login.click();
        logger.info("Нажатие на кнопку Sign in ");
        return this;
    }

    public BasePage searchProduct(String searchText){
        searhQueryTop.sendKeys(searchText);
        searhButton.click();
        logger.info("Нажатие на кнопку Поиск товара");
        return this;
    }

    public BasePage logoutAccount(){
        logout.click();
        logger.info("Нажать на кнопку -'Выйти'");
        return this;
    }


    public boolean assertUser(User user) {
        boolean rc = false;
        if(user.getLogin().equals(login.getText())) {
           logger.info("Авторизация прошла успешно");
            rc = true;
        }
        else
            logger.fatal("Ошибка авторизации");
        return rc;
    }
}
