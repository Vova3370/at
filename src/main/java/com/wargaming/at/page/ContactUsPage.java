package com.wargaming.at.page;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.Logger;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;

public class ContactUsPage {

    private static final Logger LOG = Logger.getLogger(BasePage.class);

    @FindBy(id="contact-link")
    WebElement contactLink;

    @FindBy(id="email")
    WebElement email;

    @FindBy(id="message")
    WebElement message;

    @FindBy(id="id_contact")
    WebElement idOcontact;

    @FindBy(id="submitMessage")
    WebElement submitMessage;

    @FindBy(id="contact")
    WebElement contact;

    public ContactUsPage(){
        PageFactory.initElements(getDriver(), this);
    }


    public ContactUsPage sendMesages(){
        contactLink.click();
        idOcontact.click();
        Select select = new Select(idOcontact);
        select.selectByIndex(2);
        expectedConditionVisibilityOf(email);
        email.sendKeys("dozer_8@mail.ru");
        expectedConditionVisibilityOf(message);
        message.sendKeys("Тест тест тест");
        clickSubmitMessage();
        return this;
    }


    public ContactUsPage contactclick(){
        contact.click();
        return this;
    }

    public ContactUsPage clickSubmitMessage(){
        contactLink.click();
        LOG.info("Нажать - Отправить сообщение");
        expectedConditionVisibilityOf(submitMessage);
        submitMessage.click();
        return this;
    }

}
