package com.wargaming.at.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.math.BigDecimal;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;

/**
 * Страница создание клиента
 */

public class CreateAccount {

    private static final Logger LOG = Logger.getLogger(CreateAccount.class);

    public String email = "TestMail"+RandomStringUtils.randomNumeric(3) + "@mail.ru";
    public String passwdq = "TestPasswd"+RandomStringUtils.randomNumeric(3);

    @FindBy(id="email_create")
    WebElement emailCreate;

    @FindBy(id="submitAccount")
    WebElement submitAccount;

    @FindBy(id="SubmitCreate")
    WebElement SubmitCreate;

    @FindBy(id="id_gender1")
    WebElement id_gender1;

    @FindBy(id="customer_firstname")
    WebElement customer_firstname;

    @FindBy(id="customer_lastname")
    WebElement customer_lastname;

    @FindBy(id="passwd")
    WebElement passwd;

    @FindBy(id ="address1")
    WebElement address1;

    @FindBy(id="city")
    WebElement city;

    @FindBy(id="id_state")
    WebElement uniform_state;

    @FindBy(id="postcode")
    WebElement postcode;

    @FindBy(id="other")
    WebElement other;

    @FindBy(id="phone")
    WebElement phone;

    public CreateAccount(){
        PageFactory.initElements(getDriver(), this);
    }

    public CreateAccount createNewUser(String firstName,String lastName){
        LOG.info("Cоздать новый аккаунт" + emailCreate);
        emailCreate.sendKeys(email);
        LOG.info("Нажать кнопку - 'Создать клиента'" +  SubmitCreate.getText());
        SubmitCreate.click();
        id_gender1.click();
        LOG.info("Заполнить имя" + customer_firstname.getText());
        customer_firstname.sendKeys(firstName);
        LOG.info("Заполнить фамилию" + customer_lastname.getText());
        customer_lastname.sendKeys(lastName);
        LOG.info("Заполнить пароль" + passwd.getText());
        passwd.sendKeys(passwdq);
        LOG.info("Заполнить адрес" + address1.getText());
        address1.sendKeys("Street address, P.O. Box, Company name, etc.");
        city.sendKeys("Minsk");
        uniform_state.click();
        Select select = new Select(uniform_state);
        select.selectByIndex(2);
        postcode.sendKeys("30297");
        other.sendKeys("You must register at least one phone number.");
        phone.sendKeys("375293240722");
        submitAccount.click();
        return this;
    }



}
