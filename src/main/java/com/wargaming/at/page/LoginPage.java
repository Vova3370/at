package com.wargaming.at.page;

import com.wargaming.at.dao.driver.DriverCreator;
import com.wargaming.at.model.User;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.Logger;

import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.util.ExpectedCondition.clickWhenReady;
import static com.wargaming.at.dao.util.ExpectedCondition.expectedConditionVisibilityOf;
import static org.testng.Assert.assertTrue;


public class LoginPage {

    private static final Logger logger = Logger.getLogger(LoginPage.class);

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "passwd")
    WebElement password;

    @FindBy(id = "SubmitLogin")
    WebElement SubmitLogin;

    @FindBy(id="center_column")
    WebElement centerColumn;

    public LoginPage(){
        PageFactory.initElements(getDriver(), this);
    }

    public LoginPage sign(User user){
        logger.info("Авторизация пользователя" +user);
        email.sendKeys(user.getLogin());
        password.sendKeys(user.getPassword());
        clickSubmitLogin();
        assertMyAccount();
        logger.info("Успешно.");
        return this;
    }

    public LoginPage sendEmail(User user){
        email.sendKeys(user.getLogin());
        return this;
    }

    public LoginPage clickSubmitLogin(){
        clickWhenReady(SubmitLogin);
        return this;
    }


    public LoginPage assertMyAccount(){
        logger.info("Проверка перехода на страницу пользователя ");
        expectedConditionVisibilityOf(centerColumn);
        assertTrue(getDriver().getTitle().contains("My account"));
        return this;
    }

    public LoginPage sendFailAutorization(User user){
        email.sendKeys(user.getLogin());
        password.sendKeys(user.getPassword());
        clickSubmitLogin();
        return this;
    }

}
