package com.wargaming.at.ASuite;

import com.wargaming.at.model.User;
import com.wargaming.at.page.BasePage;
import com.wargaming.at.page.LoginPage;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.apache.log4j.Logger;

import static com.wargaming.at.enums.Msg.*;
import static com.wargaming.at.model.Message.isExpectedMsgOf;

/**
 * Тесты для проверки логина
 */

public class LoginTest extends TestSuiteSetup {

    private static final Logger LOG = Logger.getLogger(LoginTest.class);


    @Test(groups = {"smoke", "acceptance"}, dataProvider = "getUser")
    public void loginTest(User user) {
        LOG.info("Тест Авторизации пользователя (позитивный сценарий)");
        new BasePage()
                .open();
        new LoginPage()
                .sign(user)
                .assertMyAccount();
        new BasePage()
                .logoutAccount();
    }

    @Test(groups = "acceptance")
    public void assertLoginEmailMessagesTest() {
        LOG.info("Тест авторизации пользователя с пустными входными данными");
        new BasePage()
                .open();
        new LoginPage()
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(EMAIL_REQUIRED));
    }

    @Test(groups = "acceptance")
    public void assertLoginPasswordMessagesTest() {
        LOG.info("Тест авторизации пользователя с пустными неведенный паролем");
        new BasePage()
                .open();
        new LoginPage()
                .sendEmail(new User("dozer_8@mail.ru", ""))
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(PASSWORD_REQUIRED));
    }

    @Test(groups = "acceptance")
    public void assertLoginAuthenticationFailedMessagesTest() {
        LOG.info("Тест авторизации пользователя с введным не верным паролем");
        new BasePage()
                .open();
        new LoginPage()
                .sendFailAutorization(new User("dozer_8@mail.ru", "testFAIL"))
                .clickSubmitLogin();
        Assert.assertTrue(isExpectedMsgOf(AUTHENTICATION_FAILD));
    }


    @DataProvider(name = "getUser")
    public static Object[] getUser() {
        return new User[]
                {new User("dozer_8@mail.ru", "vovakorzun")};
    }

}
