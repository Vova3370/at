package com.wargaming.at.ASuite;

import com.wargaming.at.page.BasePage;
import com.wargaming.at.page.CreateAccount;
import org.testng.annotations.Test;

public class CreateUserTest {

    @Test(groups = {"acceptance","smoke"})
    public void createAccount(){
        new BasePage()
                .open();
        new CreateAccount()
                .createNewUser("TestProject","TestProjectPassword");
    }

}
