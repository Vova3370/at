package com.wargaming.at.ASuite;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.wargaming.at.dao.driver.DriverCreator.close;
import static com.wargaming.at.dao.driver.DriverCreator.getDriver;
import static com.wargaming.at.dao.driver.DriverCreator.init;

public class TestSuiteSetup {

    @BeforeSuite(alwaysRun = true)
    public static void setUp() {
        init();
    }

    @AfterSuite(alwaysRun = true)
    public static void tearDown() {
        if (getDriver() != null) {
            close();
        }
    }
}



