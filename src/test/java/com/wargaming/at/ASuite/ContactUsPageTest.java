package com.wargaming.at.ASuite;

import com.wargaming.at.page.ContactUsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.wargaming.at.enums.Msg.INVALID_EMAIL_ADDRESS;
import static com.wargaming.at.enums.Msg.MESSAGE_SUCCESSFULLY;
import static com.wargaming.at.model.Message.isExpectedMsgOf;
import static com.wargaming.at.model.Message.isExpectedMsgOfS;

public class ContactUsPageTest extends TestSuiteSetup{

    @Test(groups = {"acceptance","smoke"})
    public void sendMessagesContract(){
        new ContactUsPage()
                .sendMesages();
        Assert.assertTrue(isExpectedMsgOfS(MESSAGE_SUCCESSFULLY));
        new ContactUsPage()
                .contactclick();
    }

    @Test(groups = {"acceptance","smoke"})
    public void sendSubmitMessage(){
        new ContactUsPage()
                .clickSubmitMessage();
        Assert.assertTrue(isExpectedMsgOf(INVALID_EMAIL_ADDRESS));
    }



}
