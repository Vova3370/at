package com.wargaming.at.ASuite;


import com.wargaming.at.page.BasePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

public class ProductSearchTest extends TestSuiteSetup{

    private static final Logger LOG = Logger.getLogger(ProductSearchTest.class);

    @Test(groups = {"acceptance","smoke"},dataProvider = "getProduct")
    public void SearchTest(String searchProduct){
        LOG.info("Тест поиск товара");
        new BasePage()
                .searchProduct(searchProduct);
    }

    @DataProvider(name = "getProduct")
    public static Object[] getProduct() {
        return new String []
                {"Faded Short Sleeve T-shirts"};
    }

}
